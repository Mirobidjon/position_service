package postgres

import (
	"database/sql"

	"bitbucket.org/Udevs/position_service/genproto/position_service"
	"bitbucket.org/Udevs/position_service/storage/repo"
	"github.com/google/uuid"
	"github.com/jmoiron/sqlx"
)

type professionRepo struct {
	db *sqlx.DB
}

// NewUserRepo ...
func NewProfessionRepo(db *sqlx.DB) repo.ProfessionRepoI {
	return &professionRepo{db: db}
}

func (r *professionRepo) Create(req *position_service.Profession) (string, error) {
	var (
		err error
		tx  *sql.Tx
		id  uuid.UUID
	)
	tx, err = r.db.Begin()

	defer func() {
		if err != nil {
			tx.Rollback()
		} else {
			tx.Commit()
		}
	}()

	if err != nil {
		return "", err
	}

	id, err = uuid.NewRandom()
	if err != nil {
		return "", err
	}

	query := `INSERT INTO profession (
				id,
				name
			) 
			VALUES ($1, $2) `

	_, err = tx.Exec(query, id, req.Name)

	if err != nil {
		return "", err
	}

	return id.String(), nil
}

func (r *professionRepo) GetAll(req *position_service.GetAllProfessionRequest) (*position_service.GetAllProfessionResponse, error) {
	var (
		filter      string
		args        = make(map[string]interface{})
		count       int32
		professions []*position_service.Profession
	)

	if req.Name != "" {
		filter += " AND name ilike '%' || :name || '%' "
		args["name"] = req.Name
	}

	countQuery := `SELECT count(1) FROM profession WHERE true ` + filter
	rows, err := r.db.NamedQuery(countQuery, args)
	if err != nil {
		return nil, err
	}

	for rows.Next() {
		err = rows.Scan(&count)
		if err != nil {
			return nil, err
		}
	}

	query := `SELECT
					id,
					name
				FROM profession WHERE true ` + filter

	rows, err = r.db.NamedQuery(query, args)
	if err != nil {
		return nil, err
	}

	for rows.Next() {
		var profession position_service.Profession

		err = rows.Scan(
			&profession.Id,
			&profession.Name,
		)

		if err != nil {
			return nil, err
		}

		professions = append(professions, &profession)
	}

	return &position_service.GetAllProfessionResponse{
		Professions: professions,
		Count:       count,
	}, nil

}

func (r *professionRepo) Get(id string) (*position_service.Profession, error) {
	var profession position_service.Profession

	query := `SELECT id, name FROM profession WHERE id = $1`

	row := r.db.QueryRow(query, id)
	err := row.Scan(
		&profession.Id,
		&profession.Name,
	)

	if err != nil {
		return nil, err
	}

	return &profession, nil
}
